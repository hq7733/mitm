/******************************

脚本名称: 车票票——解锁订阅

*******************************

[rewrite_local]

https://api.revenuecat.com/v1/(receipts|subscribers)/* url script-response-body https://gitlab.com/hq7733/mitm/-/raw/main/cpp.js

[mitm] 

hostname = api.revenuecat.com

*******************************/
let headers = $response.headers;

// console.log(headers);
// console.log(headers["User-Agent"]);
// $notification.post(headers["User-Agent"], headers["User-Agent"], JSON.stringify(objc));
// if(headers["User-Agent"] == "%E8%BD%A6%E7%A5%A8%E7%A5%A8/4"){
    var objc ={
      "request_date_ms": 1714218038024,
      "request_date": "2024-04-27T11:40:38Z",
      "subscriber": {
        "non_subscriptions": {
          "eticket_remove_ad_partial": [
            {
              "id": "3a3bc86990",
              "is_sandbox": false,
              "purchase_date": "2024-04-27T11:34:24Z",
              "original_purchase_date": "2024-04-27T11:34:24Z",
              "store": "app_store",
              "store_transaction_id": "60001845384963"
            }
          ]
        },
        "first_seen": "2024-04-27T11:30:38Z",
        "original_application_version": "2",
        "other_purchases": {
          "eticket_remove_ad_partial": {
            "purchase_date": "2024-04-27T11:34:24Z"
          }
        },
        "management_url": "https:\/\/apps.apple.com\/account\/subscriptions",
        "subscriptions": {
          "eticket_with_watch_6m_3d0": {
            "original_purchase_date": "2024-04-27T11:39:33Z",
            "expires_date": "2024-05-04T11:39:32Z",
            "is_sandbox": false,
            "refunded_at": null,
            "store_transaction_id": "60001845389817",
            "unsubscribe_detected_at": "2024-04-27T11:40:19Z",
            "grace_period_expires_date": null,
            "period_type": "trial",
            "purchase_date": "2024-04-27T11:39:32Z",
            "billing_issues_detected_at": null,
            "ownership_type": "PURCHASED",
            "store": "app_store",
            "auto_resume_date": null
          }
        },
        "entitlements": {
          "remove_ad_partial": {
            "grace_period_expires_date": null,
            "purchase_date": "2024-04-27T11:34:24Z",
            "product_identifier": "eticket_remove_ad_partial",
            "expires_date": null
          },
          "vip+watch_vip": {
            "grace_period_expires_date": null,
            "purchase_date": "2024-04-27T11:39:32Z",
            "product_identifier": "eticket_with_watch_6m_3d0",
            "expires_date": "2024-05-04T11:39:32Z"
          }
        },
        "original_purchase_date": "2023-09-21T04:52:37Z",
        "original_app_user_id": "$RCAnonymousID:a6adf28bff394f1abee142343fb49447",
        "last_seen": "2024-04-27T11:30:38Z"
      }
    ｝
    $notification.post("重写", "重写", JSON.stringify(objc));
    $done({body : JSON.stringify(objc)});    
// }else{
//     $notification.post("没重写", "没重写", JSON.stringify(objc));
//     $done({});    
// }

